SCELETON
--------

.. image:: https://i.postimg.cc/WbGK1wr4/sceletonpy.png


INSTALLATION
************

>>> pip install sceleton


USAGE
*****

Currently the following commands are supported:


>>> sceleton new [packagename]
"""
It will create a folder with name `[packagename]`, including a `LICENSE` file, `README.rst`,
`.gitignore`, `setup.py`, `setup.cfg`
"""


>>> sceleton new [packagename] --venv
"""
It will do the same as above, but the `package` folder will include a virtualenv folder.
"""


>>> sceleton new [packagename] --quick
"""
It will create a package folder, without asking any questions
"""


>>> sceleton new [packagename] --quick --venv
"""
It will create a package folder, without asking any questions and initializing a virtual environment.
"""


>>> sceleton init
"""
It will initialize a `setup.py` file, and if such already exists, will ask the user
for replacing.
"""


>>> sceleton init --path=path
"""
Creates a `setup.py` file at the given path, and if such file already exists, it will
ask the user for replacement.
"""


>>> sceleton init --quick
"""
Creates a `setup.py` file, wtihout asking any questions, if such file exists, will ask
the user for replacing the file
"""


>>> sceleton module [modulename]
"""
It will create a new module in the parent module directory.
"""


>>> sceleton module [modulename] --add file1.py file2.py file3.py
"""
It will create a module with modulename in the parent directory, if such doesn't already exit and will add all files given as arguments. If a file exists in the given directory, it will raise an FileExistsError.
"""


>>> sceleton module [modulename] --parentmodule=somemodule --add file1.py file2.py file3.py
"""
It will create a new module under the given parent module, and will add all files given as
arguments. If such module already exists in the parent module, it will only add given files as arguments.
"""


>>> sceleton add [packages]
"""
Adds one or more package (must be divided with interval) to the setup.py file, without
installing any of them
"""


>>> sceleton install
"""
Installs all packages found in the `setup.py` file
"""


>>> sceleton install [packages]
"""
Installs one or more packages, and adds each of them in the `setup.py` file.
If more than one package is given, it must be splitted with interval.
"""

>>> sceleton install [packages] [--no-add]
"""
Installs one or more packages, and doesn't add given packages in the `setup.py` file.
"""


>>> sceleton remove [packages]
"""
Removes one or more packages from `setup.py` file.
"""


>>> sceleton classifiers
"""
List the currently inserted classifiers in the `setup.py` file.
"""


>>> scelecton classifiers --edit
"""
Asks the user for new classifiers, erases the old chosen classifiers, and replaces them
with the new one.
"""


>>> sceleton user
"""
Shows the currently added `author` and `author_email` to the `setup.py` file.
"""


>>> sceleton user --edit
"""
Asks the user for new `author` and `email`.
"""


>>> sceleton license
"""
Shows the currently chosen license.
"""


>>> sceleton license --edit
"""
Asks the user for a new license.
"""


>>> sceleton build
"""
Calls the `python3 setup.py buid`
"""

>>> sceleton sdist
"""
Calls the `python3 setup.py sdist`
"""


>>> sceleton upload
"""
Uploads the built package to `pypi` using `twine upload dist/*` command.
"""


>>> sceleton local
"""
Installs the package localy. If venv is activated, will install the package to current env, otherwise will install it to where `python3` is installed. It uses `python3 setup.py install` command.
"""


LICENSE
*******

`MIT License <https://github.com/monzita/sceleton/blob/master/LICENSE>`_

UPDATES
*******

**27/11/2019 v0.0.6**

>>> fix some variable usage errors, so now calling ``init`` command should work, without any problems.


**13/11/2019 v0.0.5**

>>> `build` now calls `python(3) setup.py build`, and `sdist` is added for `python(3) setup.py sdist`

>>> All commands now, which use `python` as command, check for default python version.


**12/11/2019 v0.0.5**

>>> New option ``--debug`` is added, so when some error occur, user can check the whole stack trace.

>>> ``venv`` is added as command, so now after creation of the project, new virtualenv can be created.

>>> Each command now has its own file, in the folder commands. (sceleton > commands)

>>> Some style fixes.


**13/10/2019 v0.0.4**

>>> Initializing/Adding/Removing packages is fixed.



**13/10/2019 v0.0.3**

>>> Now installing of packages should work without specifying names of any package.

>>> Virtualenv is removed from `setup.py` as needed dependency from the package, because by default python's venv is used.