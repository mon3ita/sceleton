Sceleton > Strucutre > Project
------------------------------

>>> classifier

Formats the chosen classifiers.

>>> git

Initializes git repository.

>>> license

Initializes a new license file.

>>> package

Installs all packages, given in a list.

>>> project

Creates all files in the project - `setup.py`, `README.rst`, `project` folder, `LICENSE`.

>>> readme

Initializes a README file.

>>> setup_py

Initializes a `setup.py file`, or modifies it.

>>> venv

Initializes a new venv environment.