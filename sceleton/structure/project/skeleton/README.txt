[projectname-title]
------------

Something about the project

Installation
************

>>> pip install [projectname]

Documentation
*************

Add `link <add-link-to-doc-here>`_ to it.

Example
*******

Example usage of [projectname]


Code of conduct
***************


LICENSE
*******

`[license] <add-link-to-license-here>`_